
// Session 35 - Data Persistence via Mongoose ODM

console.log("Session 35 - Data Persistence via Mongoose ODM");
// npm init / npm init - y
// npm install express
// npm install mongoose

// -----------------------------
// A "module" is a software component or part of a program that contains one or more routines
// it also allows us to access methods and functions to easilty create a server we store our express module to a variable so we could easily access its keywords

const express = require("express");
 
const app = express();

const port = 3001;
const mongoose = require("mongoose");


// setup for allowing the server to handle data from requests
// Allows your app to road json data

app.use(express.json());

// allows your app to read data from forms

app.use(express.urlencoded({extended:true}))

//reference: https://dev.to/griffitp12/express-s-json-and-urlencoded-explained-1m7o

mongoose.connect("mongodb+srv://admin:admin@batch218-to-do.c4bzfiz.mongodb.net/toDo?retryWrites=true&w=majority",

{

	useNewurlParser: true,
	useUnifiedTopology: true

});


//[SECTION] Creating Schema

// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Syntax:
/*
	const schemaName = new mongoose.Schema({<keyvalue:pair>});
*/
// name & status
// "required" is used to specify that a field must not be empty.
// "default" is used if a field value is not supplied.


/*const taskSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true,"Task name is required"]
	},
	status:{
		type: String,
		default: "pending"
	}
})
*/
//[SECTION] Models
// The variable/object "Task"can now used to run commands for interacting with our database
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
	//model name		//collection name    //schemaName
//const Task = mongoose.model("Task", taskSchema);

// -----------------------------------------------




/*




// [SECTION] POST / INSERT
// first parameter is for request, second parameter is for response
app.post("/tasks",(req, res) =>{
	console.log(req.body.name);

	// Goal ; check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of mongoDB
	//findOne() returns the first document that matches the search criteria
	// if there are no matches, the value of result is null
	//"err" is a shorthand naming convention for errors

	Task.findOne({name: req.body.name}, (err, result) => {
			if(result != null && result.name == req.body.name){
				// Return a message to the client/Postman
				return res.send("Duplicate task found")
			}
				// if no document was found
			else {
				//this is where we receive our field values
				let newTask = new Task ({
					name: req.body.name
				});
				// the "save" method will store the information to the database
				newTask.save((saveErr, savedTask) => {
					if(saveErr){
							return console.error(saveErr)
					}
					else {
						return res.status(201).send("New task created");
					}
				})
			}
	})
})

app.get("/tasks", ( req, res) => {
	//model
	Task.find({name: "organize files"}, (err,result) =>{
		if(err){

			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
});




*/






// ACTIVITY
/*
	1. Create a User schema.
		username - string
		password - string
	2. Create a User model.
	Take a screenshot of your mongoDB collection to show that the users collection is added.
*/

//code here
const userSchema = new mongoose.Schema({
	username:{
		type: String,
		
	},
	password:{
		type: String,
		
	}	
})

const User = mongoose.model("User", userSchema);





/*
	3. Create a route for creating a user, the endpoint should be “/signup” and the http method to be used is ‘post’.
*/

/*
	4. Use findOne and conditional statement to check if there is already an existing username. If there is already, the program should send a response “Duplicate user found.” If there is no match  (else), it should successfully create a new user with password.

5. If there is no error encountered during the process (include status code 201), the program should send a response “New user registered”



*/
app.post("/signup",(req, res) =>{
	console.log(req.body.username);


	User.findOne({username: req.body.username}, (err, result) => {
			if(result != null && result.username == req.body.username){
				// Return a message to the client/Postman
				return res.send("Duplicate user found")
			}
				
			else {
				
				let newUser = new User ({
					username: req.body.username,
					password: req.body.password
				});
				
				newUser.save((saveErr, savedTask) => {
					if(saveErr){
							return console.error(saveErr)
					}
					else {
						return res.status(201).send("New user registered");
					}
				})
			}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));